FROM python

ADD update.py /srvr/
RUN pip3 install requests


ENTRYPOINT ["python", "/srvr/update.py"]

